package br.erasmo.icaro.node;

import br.erasmo.icaro.table.State;

public class Node {
	
	private State state;
	
	private Node next;
	private Node previous;
	
	private int gVal;
	private int hVal;
	private String step;

	public Node(State state) { // Constructor for initial state
		this.state = state;
		this.gVal = 0;
	}
	
	public Node(Node previous, State state,
			String step) {
		this.previous = previous;
		this.state = state;
		this.step = step;
		this.gVal = previous.getGVal()+1;
	}
	
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	public void setPrevious(Node previous) {
		this.previous = previous;
	}
	public Node getPrevious() {
		return previous;
	}
	public Integer getGVal() {
		return gVal;
	}
	
	public int getHVal() {
		return hVal;
	}

	public void setHVal(int hVal) {
		this.hVal = hVal;
	}
	
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	
	@Override
	public String toString() {
		return (step != null?step:"")+"={"+System.lineSeparator()+(state != null ? state.toString():"")+System.lineSeparator()+"}";
	}
}
