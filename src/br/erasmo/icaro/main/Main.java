package br.erasmo.icaro.main;

import java.util.Scanner;

import br.erasmo.icaro.node.Node;
import br.erasmo.icaro.table.State;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException {
		
		Scanner s = new Scanner(System.in);
		String message = "1 - Estado Aleatório"+System.lineSeparator();
		message += "2 - Inserir caso de testes do console"+System.lineSeparator();
		System.out.println(message);
		char op = s.next().charAt(0); 
		switch(op) {
			case '1':
				runRandomState();
				break;
			case '2':
				Integer[] values = new Integer[State.ARRAY_DIMENSION*State.ARRAY_DIMENSION];
				for(int i = 0 ; i < values.length ; i++) {
					values[i] = s.nextInt();
				}
				runCaseFromInput(values);
				break;
		}
		s.close();
	}

	private static void runRandomState() {
		Executor exec = new Executor(State.RANDOM_STATE());
		Node finalRoute = exec.start();
		if(finalRoute != null) {
			printState(buildRoute(finalRoute));
			System.out.println();
			System.out.println("Número de movimentos: "+finalRoute.getGVal());
		}else {
			System.out.println("Solução não encontrada.");
		}
	}
	
	private static void runCaseFromInput(Integer[] values) {
		Executor exec = new Executor(State.caseTest(values));
		Node finalRoute = exec.start();
		if(finalRoute != null) {
			printState(buildRoute(finalRoute));
			System.out.println();
			System.out.println("Número de movimentos: "+finalRoute.getGVal());
		}else {
			System.out.println("Solução não encontrada.");
		}
	}
	
	public static Node buildRoute(Node next) {
		
		if(next.getPrevious() == null) {
			return next;
		}
		next.getPrevious().setNext(next);
		return buildRoute(next.getPrevious());
	}
	
	public static void printState(Node node) {
		if(node.getStep() != null) {
			System.out.println("Step: "+node.getStep());
		}
		System.out.println(node.getState());
		if(node.getNext() != null) {
			System.out.println();
			printState(node.getNext());
		}
	}
}
