package br.erasmo.icaro.main;

import static br.erasmo.icaro.main.util.Util.addIfIsNotPresent;
import static br.erasmo.icaro.main.util.Util.indexOf;
import static br.erasmo.icaro.main.util.Util.isNodeInList;
import static br.erasmo.icaro.main.util.Util.removeFromList;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import br.erasmo.icaro.node.Node;
import br.erasmo.icaro.table.AStar;
import br.erasmo.icaro.table.Move;
import br.erasmo.icaro.table.State;

public class Executor {
	
	private Node initialNode;
	private List<Node> visitedNodes;
	private List<Node> nodesToVisit;
	
	public Executor(State initialState){
		this.initialNode = new Node(initialState);
		this.visitedNodes = new ArrayList<>();
		this.nodesToVisit = new ArrayList<>();
	}
	
	public Node start(){
		System.out.println("Estado Inicial");
		System.out.println(initialNode.getState());
		System.out.println("-----------------------");
		return exec();
	}
	
	private Node exec() {
		
		Timer timer = startLoadingIndicator();
		
		Node node = this.initialNode;
		
		while(nodesToVisit.size() > 0 || node.equals(this.initialNode)) {
			addIfIsNotPresent(node, visitedNodes);
			removeFromList(node, nodesToVisit);
			
			if(node.getState().equals(State.FINAL_STATE())) {
				timer.cancel();
				System.out.println("Solução encontrada. "+visitedNodes.size()+" estados analisados.");
				return node;
			}
			
			Move move = new Move(node);
			List<Node> generated = move.get();
			
			AStar aStar = new AStar(generated);
			aStar.calc();
			
			addToVisit(generated);
			
			Node nextNode = findBest(nodesToVisit);
			
			if(nextNode != null) {
				//System.out.println(nextNode);
				//System.out.println();
				node = nextNode;
			}
		}
		
		timer.cancel();
		return null;
	}
	
	private Timer startLoadingIndicator() {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			int n = 0;
			
			@Override
			public void run() {
				
				switch(n) {
					case 0:
						System.out.print(".  \r");
						break;
					case 1:
						System.out.print(".. \r");
						break;
					case 2:
						System.out.print("...\r");
						n = -1;
						break;
				}
				n++;
			}
			
		}, 0, 1000);
		return timer;
	}
	
	private Node findBest(List<Node> nodes){
		Node best = null;
		
		for(Node node : nodes) {
			if(best != null) {
				if(node.getGVal()+node.getHVal() <
				best.getGVal()+best.getHVal()) {
					best = node;
				}
			}else {
				best = node;
			}
		}
		return best;
	}
	
	private void addToVisit(List<Node> toAdd) {
		for(int i = 0; i < toAdd.size(); i++) {
			Node node = toAdd.get(i);
			boolean isInVisited = isNodeInList(node, visitedNodes);
			boolean isInToVisit = isNodeInList(node, nodesToVisit);
			
			if(!isInVisited && !isInToVisit) {
				addIfIsNotPresent(node, nodesToVisit);
			}else if(isInToVisit) {
				int index = indexOf(node, nodesToVisit);
				Node found = nodesToVisit.get(index);
				
				if(found.getGVal() >= node.getGVal()) {
					nodesToVisit.remove(index);
					addIfIsNotPresent(node, nodesToVisit);
				}
			}
		}
	}
}
