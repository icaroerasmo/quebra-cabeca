package br.erasmo.icaro.main.util;

import java.util.List;

import br.erasmo.icaro.node.Node;

public class Util {
	public static boolean isNodeInList(Node node, List<Node> list){
		for(int i = 0; i < list.size(); i++) {
			Node n = list.get(i);
			if(n.getState().equals(node.getState())) {
				return true;
			}
		}
		return false;
	}
	
	public static int indexOf(Node node, List<Node> list) {
		for(int i = 0; i < list.size(); i++) {
			Node n = list.get(i);
			if(node.getState().equals(n.getState())) {
				return i;
			}
		}
		return -1;
	}
	
	public static void addIfIsNotPresent(Node node, List<Node> list) {
		if(!isNodeInList(node, list)) {
			list.add(node);
		}
	}
	
	public static void addIfIsNotPresent(List<Node> nodesToAdd, List<Node> list) {
		for(int i = 0; i < nodesToAdd.size(); i++) {
			addIfIsNotPresent(nodesToAdd.get(i), list);
		}
	}
	
	public static void removeFromList(Node node, List<Node> list) {
		int index = indexOf(node, list);
		
		if(index > -1) {
			list.remove(index);
		}
	}
}
