package br.erasmo.icaro.table;

import java.util.ArrayList;
import java.util.List;

import br.erasmo.icaro.node.Node;

public class Move {
	
	private Node node;
	
	public Move(Node node){
		this.node = node;
	}
	
	private State left() {
		State newState = node.getState().clone();
		Piece[][] table = newState.get();
		for(int x = 0 ; x < table.length ; ++x) {
			for(int y = 0 ; y < table[x].length ; ++y) {
				if(table[x][y].getId() == 9 && y < table[x].length-1) {
					Piece tmp = table[x][y+1];
					table[x][y+1] = table[x][y];
					table[x][y] = tmp;
					return newState;
				}
			}
		}
		return null;
	}
	
	private State right() {
		State newState = node.getState().clone();
		Piece[][] table = newState.get();
		for(int x = 0 ; x < table.length ; ++x) {
			for(int y = 0 ; y < table[x].length ; ++y) {
				if(table[x][y].getId() == 9 && y > 0) {
					Piece tmp = table[x][y-1];
					table[x][y-1] = table[x][y];
					table[x][y] = tmp;
					return newState;
				}
			}
		}
		return null;
	}
	
	private State up() {
		State newState = node.getState().clone();
		Piece[][] table = newState.get();
		for(int x = 0 ; x < table.length ; ++x) {
			for(int y = 0 ; y < table[x].length ; ++y) {
				if(table[x][y].getId() == 9 && x < table[x].length-1) {
					Piece tmp = table[x+1][y];
					table[x+1][y] = table[x][y];
					table[x][y] = tmp;
					return newState;
				}
			}
		}
		return null;
	}
	
	private State down() {
		State newState = node.getState().clone();
		Piece[][] table = newState.get();
		for(int x = 0 ; x < table.length ; ++x) {
			for(int y = 0 ; y < table[x].length ; ++y) {
				if(table[x][y].getId() == 9 && x > 0) {
					Piece tmp = table[x-1][y];
					table[x-1][y] = table[x][y];
					table[x][y] = tmp;
					return newState;
				}
			}
		}
		return null;
	}
	
	public List<Node> get(){
		List<Node> nodes = new ArrayList<>();
		State left = left();
		State right = right();
		State up = up();
		State down = down();
		if(left != null) {
			nodes.add(new Node(node, left, "Left"));
		}
		if(right != null) {
			nodes.add(new Node(node, right, "Right"));
		}
		if(up != null) {
			nodes.add(new Node(node, up, "Up"));
		}
		if(down != null) {
			nodes.add(new Node(node, down, "Down"));
		}
		return nodes;
	}
}
