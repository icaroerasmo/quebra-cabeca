package br.erasmo.icaro.table;

import java.util.Formatter;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import br.erasmo.icaro.node.Node;

public class State {
	
	public static final int ARRAY_DIMENSION = 3;
	private Piece[][] table;
	
	private State(){
		table = new Piece[ARRAY_DIMENSION][ARRAY_DIMENSION];
	}
	
	private State(Piece[][] table) {
		this.table = table;
	}
	
	protected Piece[][] get() {
		return table;
	}
	
	private void setState(Piece[][] table) {
		this.table = table;
	}
	
	public static final State RANDOM_STATE() {
		State random = shuffle();
		return random;
	}
	
	public boolean isFinalState() {
		return this.equals(FINAL_STATE());
	}
	
	public static State FINAL_STATE(){
		State finalState = new State();
		Piece [][] table = finalState.get();
		
		int index = 0;
		
		for(int x = 0 ; x < table.length ; ++x) {
			for(int y = 0 ; y < table[x].length ; ++y) {
				table[x][y] = new Piece(++index);
			}
		}
		
		finalState.setState(table);
		return finalState;
	}
	
	@Override
	public State clone() {
		State state = new State();
		Piece[][] clone = state.get();
		for(int x = 0 ; x < this.table.length ; ++x) {
			for(int y = 0 ; y < this.table[x].length ; ++y) {
				if(this.table[x][y] != null) {
					clone[x][y] = this.table[x][y].clone();
				}
			}
		}
		return state;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null || !(obj instanceof State)) {
			return false;
		}
		
		Piece[][] table = ((State) obj).get();
		
		if(table == null) {
			return false;
		}
		
		for(int x = 0; x < table.length; ++x) {
			for(int y = 0; y < table[x].length; ++y){
				if(!table[x][y].equals(this.table[x][y])) {
					return false;
				};
			}
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		Formatter fmt = new Formatter();
		for(Piece[] x : table) {
			String buff = "";
			for(Piece y : x) {
				buff += (y.getId() != 9 ? y.getId():" ");
				if(!y.equals(x[ARRAY_DIMENSION-1])) {
					buff += " ";
				}
			}
			fmt.format(buff);
			if(!x.equals(table[ARRAY_DIMENSION-1])) {
				fmt.format(System.lineSeparator());
			}
		}
		String toString = fmt.toString();
		fmt.close();
		return toString;
	}
	
	private static State shuffle() {
		Node initial = new Node(FINAL_STATE());
		Node current = null;
		
		for(int i = 0 ; i < randomValue(1000, 2000) ; i++) {
			if(current == null) {
				current = initial;
			}
			
			Move moves = new Move(current);
			List<Node> nodes = moves.get();
			
			current = nodes.get(randomValue(0, nodes.size()-1));
		}
		
		return current.getState();
	}

	private static int randomValue(int min, int max) {
	 return	ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	public static State caseTest(Integer[] positions){
		Piece[][] caseTest = new Piece[ARRAY_DIMENSION][ARRAY_DIMENSION];
		
		int n = 0;
		
		for(int i = 0; i < ARRAY_DIMENSION; i++) {
			for(int j = 0; j < ARRAY_DIMENSION; j++) {
				caseTest[i][j] = new Piece(positions[n++]);
			}
		}
		
		return new State(caseTest);
	}
	
	public static State caseTest(){
		Piece[][] caseTest = new Piece[][]{
				{new Piece(4),new Piece(7),new Piece(8)},
				{new Piece(9),new Piece(3),new Piece(2)},
				{new Piece(5),new Piece(6),new Piece(1)}
				};
		return new State(caseTest);
	}
}
