package br.erasmo.icaro.table;

import java.util.List;

import br.erasmo.icaro.node.Node;

public class AStar {
	
	private List<Node> toVisit;
	
	public AStar(List<Node> toVisit){
		this.toVisit = toVisit;
	}
	
	public void calc() {
		for(Node n : toVisit) {
			int currentVal = manhattanDistance(n.getState());
			n.setHVal(currentVal);
		}
	}
	
	private int manhattanDistance(State state) {
		Piece[][] table = state.get();
		State finalState = State.FINAL_STATE();
		
		int n = 0;
		
		//Manhattan Distance
		for(int y = 0 ; y < table.length ; y++) {
			for(int x = 0 ; x < table[y].length ; x++) {
				if(!finalState.get()[y][x].equals(table[y][x]) &&
						table[y][x].getId() != 9) {
					int py = (table[y][x].getId()-1)/3;
					int px = table[y][x].getId()-py*3-1;
					int d = Math.abs(x-px) + Math.abs(y-py);
					n += d;
				}
			}
		}
		return n;
	}
}