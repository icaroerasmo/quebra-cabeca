package br.erasmo.icaro.table;

public class Piece {
	
	private int id;
	
	public Piece(int id){
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	@Override
	protected Piece clone(){
		Piece piece = new Piece(this.id);
		return piece;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Piece)) {
			return false;
		}
		return ((Piece)obj).getId() == this.id;
	}
}